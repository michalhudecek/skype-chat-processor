const fs = require("fs")

const rawdata = fs.readFileSync("input/messages.json")
const alldata = JSON.parse(rawdata)
const messages = alldata.conversations[0].MessageList

const filteredMessages = messages.filter(
    (message) => message.from == "8:live:andrei.nikiti"
)

const attendanceDates = filteredMessages.map((message) => {
    const datum = new Date(message.originalarrivaltime)
    return `${datum.getFullYear()}-${datum.getMonth() + 1}-${datum.getDate()}`
})

const uniqueAttendanceDates = new Set(attendanceDates)

console.log(uniqueAttendanceDates)
fs.writeFile(
    "output/attendance.json",
    JSON.stringify(Array.from(uniqueAttendanceDates)),
    function (err) {
        if (err) return console.log(err)
        console.log(uniqueAttendanceDates.size)
    }
)
