const fs = require("fs")

const rawdata = fs.readFileSync("input/messages.json")
const alldata = JSON.parse(rawdata)
const messages = alldata.conversations[0].MessageList

const filteredMessages = messages.filter(
    (message) => message.from == "8:live:andrei.nikiti"
)

fs.writeFile(
    "output/full-log.json",
    JSON.stringify(filteredMessages),
    function (err) {
        if (err) return console.log(err)
        console.log(filteredMessages.length)
    }
)
